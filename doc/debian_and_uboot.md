The normal boot of debian on raspberry pi is through `bootcode.bin`. Using
`config.txt` the kernel and initramfs file names are defined and both are taken
from fat partition.

Mender uses uboot as main bootloader. The mender-client configure variables
that uboot can read and this way it changes the boot partition. To change the
boot process and boot uboot instead of the kernel is enough with erase the
kernel configurations, this will made the bootcode to default to `kernel.img`,
and copy the uboot.bin there (the bootcode will search for the highest number
of kernel.img, we copy the mender provided uboot in `kernel7.img`). To do some
debugging we enable the uart on GPIO14 (tx) and GPIO15 (rx) doing:

```bash
$ sed -i -e "s/BOOT_UART=0/BOOT_UART=1/" bootcode.bin
```

And enabling the uart for the rest of the boot process adding to `config.txt`:
```bash
...
enable_uart=1
...
```

With this we can see that the bootcode loads the uboot and that the bootscr try
to boot a kernel that doesn't exist. Stooping the uboot and trying to load the
correct debian kernel doesn't help. The problem is that the provided mender
uboot is compiled on 32bits and the debian kernel is 64 bits.

To make a uboot compiled on 64bits we clone the mender fork from
https://github.com/mendersoftware/uboot-mender.git. This fork adds the
variables needed by mender-client.

```bash
uboot-mender/$ ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make rpi_3_defconfig
uboot-mender/$ ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make menuconfig
uboot-mender/$ ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- make
```
Compiling with `gcc version 10.2.0 (Debian 10.2.0-9)` there is some error that
can by fix with:

```bash
diff --git a/scripts/dtc/dtc-lexer.l b/scripts/dtc/dtc-lexer.l
index fd825ebba6..f57c9a7e83 100644
--- a/scripts/dtc/dtc-lexer.l
+++ b/scripts/dtc/dtc-lexer.l
@@ -38,7 +38,7 @@ LINECOMMENT   "//".*\n
 #include "srcpos.h"
 #include "dtc-parser.tab.h"

-YYLTYPE yylloc;
+extern YYLTYPE yylloc;
 extern bool treesource_error;

 /* CAUTION: this will stop working if we ever use yyless() or yyunput() */
```

The `bootcode.bin` also is provided by mender. For some reason this needs the
bcm2710 device tree to be load. So we deleted all debian device tree files and
copy only the `bcm2710-rpi-3-b.dtb`. With this we make the kernel to boot:

```bash
U-Boot> fdt addr ${fdt_addr_r}
U-Boot> fdt get value bootargs /chosen bootargs
U-Boot> load mmc 0 ${kernel_addr_r} vmlinuz-4.19.0-12-arm64 
U-Boot> booti ${kernel_addr_r} - ${fdt_addr_r}
```

But without a initramfs the kernel stop waiting for the rootfs. This is because
the debian kernel needs the initramfs to load the kernel modules and initialize
the system. According with the documentation
(https://www.debian.org/releases/stable/arm64/ch05s01.en.html), loading the
device tree, the kernel and the ramdisk in three different location with enough
space must do it:

```bash
U-Boot> setenv fdt_addr_r 0x10000000
U-Boot> setenv kernel_addr_r 0x20000000
U-Boot> setenv ramdisk_addr_r 0x30000000
U-Boot> load mmc 0 ${fdt_addr_r} bcm2710-rpi-3-b.dtb
U-Boot> fdt addr ${fdt_addr_r}
U-Boot> fdt get value bootargs /chosen bootargs
U-Boot> mmc dev mmc 0 
U-Boot> load mmc 0 ${kernel_addr_r} vmlinuz-4.19.0-12-arm64 
U-Boot> load mmc 0 ${ramdisk_addr_r} initrd.img-4.19.0-12-arm64
U-Boot> booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}
```

But in this case the kernel looks like is booting but we cannot see any output.
This is because the bootargs from `config.txt` aren't added to the ones defined
on the device tree. Doing the concatenation manually seems to work:

```bash
U-Boot> setenv fdt_addr_r 0x10000000
U-Boot> setenv kernel_addr_r 0x20000000
U-Boot> setenv ramdisk_addr_r 0x30000000
U-Boot> load mmc 0 ${fdt_addr_r} bcm2710-rpi-3-b.dtb
U-Boot> fdt addr ${fdt_addr_r}
U-Boot> fdt get value bootargs /chosen bootargs
U-Boot> setenv bootargs ${bootargs} loglevel=7 console=ttyS0,115200 root=/dev/mmcblk0p2 rw elevator=deadline fsck.repair=yes net.ifnames=0 cma=64M rootwait
U-Boot> mmc dev mmc 0 
U-Boot> load mmc 0 ${kernel_addr_r} vmlinuz-4.19.0-12-arm64 
U-Boot> load mmc 0 ${ramdisk_addr_r} initrd.img-4.19.0-12-arm64
U-Boot> booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}
```

Now to make it working with mender we make a `boot.scr`:

```bash
setenv fdt_addr_r 0x10000000
setenv kernel_addr_r 0x20000000
setenv ramdisk_addr_r 0x30000000

load mmc 0 ${fdt_addr_r} bcm2710-rpi-3-b.dtb

fdt addr ${fdt_addr_r}
fdt get value bootargs /chosen bootargs

run mender_setup
mmc dev ${mender_uboot_dev}

setenv bootargs ${bootargs} console=ttyS0,115200 root=${mender_kernel_root} rw elevator=deadline fsck.repair=yes net.ifnames=0 cma=64M rootwait

if load ${mender_uboot_root} ${kernel_addr_r} /vmlinuz; then
    if load ${mender_uboot_root} ${ramdisk_addr_r} /initrd; then
        booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}
    else
        echo "No initramfs found."
    fi
else
    echo "No bootable Kernel found."
fi
run mender_try_to_recover
# Recompile with:
# mkimage -C none -A arm -T script -d boot.cmd boot.scr
```
