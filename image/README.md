# Miniserver base image creator

This image generator use vmdb2 to generate a image following a series of steps
defined on the steps file (./vmdb2_config/). Some steps are made on the host
system and some on the target image using qemu.

Based on https://salsa.debian.org/raspi-team/image-specs

## Image build

The image build is configured using *config.yml*. The available variables are:

| Variable | Default | Description |
| :---: | :---: | :--- |
| img_name | miniserver | Output image name |
| build_dir | ./build/ | Output dir |
| hostname | miniserver | Miniserver hostname|
| user.name | admin | First user name |
| user.pass | 1234 | Password for first user |
| wpa_supplicant.ssid | None | wpa_supplicant ssid configuration |
| wpa_supplicant.psk | None | wpa_supplicant psk configuration |
| locales.lang | en_gb.utf-8 | Locale language configuration |
| locales.layout | gb | Keyboard layout configuration |
| locales.timezone | Europe/London | Timezone configuration|

### Prepare the build environment

The build system is base python3, parted and vmdb2 so you will need that
on a Debian OS.

```bash
$ sudo apt install python3 python3-pip vmdb2 make parted
```

All the requirements are defined on requirements.txt. Using virtialenv:

```bash
$ python3 -m venv env
$ source env/bin/activate
(env)$ pip3 install -r requirements.txt
```

### Build the image

Just run the build.py script inside the environment. Sudo is used for running
vmdb2 as root, you will be asked for your password.

```bash
(env)$ python build.py --verbose
```

### Convert Image

The image must be converted using the mender specific tool mender-convert. There is a script that use this tool 
and perform some other actions.

```bash
(env)$ bash scripts/convert_image.sh
```

### Image deploy on sd

Ensure to use the correct device:

```bash
$ sudo dd if=build/base_image_v${release}.img of=/dev/mmcblk0 bs=64k status=progress
```

### Image release deploy

When a new release is build this must be deployed to the fileserver so it is
available for everyone.

```bash
$ rsync -SPz build/base_image_v${new_release}.img repo.librerouter.org:/var/www/librerouter/miniserver/base_image/
$ rsync -SPz build/artifact_v${new_release}.mender repo.librerouter.org:/var/www/librerouter/miniserver/artifacts/
```

## Image build (Docker)

### Build the docker environment

```bash
$ cd image
$ build -t miniserver-build .
```

### Build the image

Run the docker image and run the same commands as the manual way:

```bash
$ docker run --privileged -v `pwd`:/home -v /dev:/dev --rm -it miniserver-build
(docker)$ cd /home
(docker)$ python3 build.py --verbose
(docker)$ bash scripts/convert_image.sh
```
