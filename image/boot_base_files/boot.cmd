setenv kernel_addr_r 0x20000000
setenv ramdisk_addr_r 0x30000000

load mmc 0 ${fdt_addr_r} bcm2837-rpi-3-b.dtb

fdt addr ${fdt_addr_r}
fdt get value bootargs /chosen bootargs

run mender_setup
mmc dev ${mender_uboot_dev}

setenv bootargs "bcm2708_fb.fbwidth=656 bcm2708_fb.fbheight=416 bcm2708_fb.fbswap=1 dma.dmachans=0x7f35 bcm2709.boardrev=0xa02082 bcm2709.serial=0x3f568d48 bcm2709.uart_clock=48000000 smsc95xx.macaddr=B8:27:EB:56:8D:48 vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  console=tty0 console=ttyS1,115200 root=${mender_kernel_root} rw elevator=deadline fsck.repair=yes net.ifnames=0 cma=64M rootwait"

if printenv environment_init; then
    echo "Environment already initialized"
else
    echo "Initializing environment"
    setenv environment_init 1
    saveenv
fi

if load ${mender_uboot_root} ${kernel_addr_r} /vmlinuz; then
    if load ${mender_uboot_root} ${ramdisk_addr_r} /initrd.img; then
        booti ${kernel_addr_r} ${ramdisk_addr_r}:${filesize} ${fdt_addr_r}
    else
        echo "No initramfs found."
    fi
else
    echo "No bootable Kernel found."
fi
run mender_try_to_recover
# Recompile with:
# mkimage -C none -A arm -T script -d boot.cmd boot.scr
