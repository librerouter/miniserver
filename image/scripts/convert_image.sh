#!/bin/bash
source scripts/yaml.sh
set -ex

create_variables config.yaml

img_name=`echo ${img_name} | sed 's/"//g'`
build_dir=`echo ${build_dir} | sed 's/"//g'`
release_version=`echo ${release_version} | sed 's/"//g'`

orig_path=`pwd`
mender_dir=${build_dir}/mender_convert/
mender_base_name=${img_name}-raspberrypi3-mender
mender_image_name=${mender_base_name}.img
mender_artifact_name=${mender_base_name}.mender
mender_rootfs_name=${mender_base_name}.rootfs

# Clone mender convert
if [ ! -d ${mender_dir} ]; then
    git clone --branch 2.2.0 https://github.com/mendersoftware/mender-convert.git ${mender_dir}
fi

# Build docker and convert image
echo "Convert image with mender-convert"
cp ${build_dir}/${img_name}.img ${mender_dir}
cd ${mender_dir}
MENDER_ARTIFACT_NAME=snapshot_${release_version} ./docker-mender-convert --disk-image ${img_name}.img --config configs/raspberrypi3_config

# Copy hacked boot files
cd deploy
gunzip ${mender_image_name}.gz
mkdir -p img_mnt/

dev_loop=`losetup -f -P --show ${mender_image_name}`

echo "Change mender convert defaults to miniserver customs"
mount ${dev_loop}p1 img_mnt/
cp ${orig_path}/boot_base_files/* img_mnt/
umount -l img_mnt/

echo "Delete mender customs fw-tools"
mount ${dev_loop}p2 img_mnt/
rm img_mnt/usr/sbin/fw_printenv
rm img_mnt/usr/sbin/fw_setenv
ln -s /usr/bin/fw_printenv img_mnt/usr/sbin/fw_printenv
ln -s /usr/bin/fw_setenv img_mnt/usr/sbin/fw_setenv
umount -l img_mnt/

mount ${dev_loop}p3 img_mnt/
rm img_mnt/usr/sbin/fw_printenv
rm img_mnt/usr/sbin/fw_setenv
ln -s /usr/bin/fw_printenv img_mnt/usr/sbin/fw_printenv
ln -s /usr/bin/fw_setenv img_mnt/usr/sbin/fw_setenv
umount -l img_mnt/

dd if=${dev_loop}p2 of=${mender_rootfs_name}

# Clean
losetup -d ${dev_loop}
cd ${orig_path}

# Move complete image and artifact
mv ${mender_dir}/deploy/${mender_image_name} ${build_dir}/base_image_${release_version}.img
mender-artifact write rootfs-image -t raspberrypi3 \
                                   -n ${release_version} \
                                   -f ${mender_dir}/deploy/${mender_rootfs_name} \
                                   -o ${build_dir}/artifact_${release_version}.mender
