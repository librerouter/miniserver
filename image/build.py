import argparse
import glob
import jinja2
import logging
import pathlib
import subprocess
import shutil
import sys
import yaml



BASE_FILES_DIRNAME = 'base_files'
BINARY_BASE_FILES_DIRNAME = 'boot_base_files'
MINISERVER_YAML = 'miniserver.yaml'


def load_vmdb2_steps(config):
    logging.info('Loading vmdb2 config templates')
    vmdb2_template_loader = jinja2.FileSystemLoader('./vmdb2_config/')
    vmdb2_templates = jinja2.Environment(loader=vmdb2_template_loader)

    vmdb2_config = {'steps': []}
    for template in vmdb2_templates.list_templates():
        if '.swp' in template:
            continue
        logging.info(f'Loading template {template}')
        steps_template = vmdb2_templates.get_template(template)

        logging.debug(f'Rendering templeate {template} with config variables')
        new_config = yaml.load(steps_template.render(config),
                               Loader=yaml.FullLoader)
        vmdb2_config['steps'].extend(new_config['steps'])
    return vmdb2_config


def render_base_files(config, build_base_files_dir_path):
    logging.info('Loading base files templates')
    base_files_loader = jinja2.FileSystemLoader('./base_files/')
    base_files_templates = jinja2.Environment(loader=base_files_loader)

    for base_file_name in base_files_templates.list_templates():
        if '.swp' in base_file_name:
            continue
        logging.info(f'Loading template {base_file_name}')
        template = base_files_templates.get_template(base_file_name)
        render_stream = template.stream(config)

        base_file_path = build_base_files_dir_path.joinpath(base_file_name)
        if base_file_path.exists():
            base_file_path.unlink()
        base_file_path.touch()

        with base_file_path.open('w') as fp:
            render_stream.dump(fp)


def prepare_build_dir(build_dir_path_name):
    build_dir = pathlib.Path(build_dir_path_name)
    if not build_dir.exists():
        logging.debug(f'Creating build directory {build_dir.absolute()}')
        build_dir.mkdir()

    base_files_dir = build_dir.joinpath(BASE_FILES_DIRNAME)
    if not base_files_dir.exists():
        logging.debug(
            f'Creating base files directory {base_files_dir.absolute()}')
        base_files_dir.mkdir()

    boot_base_files_dir = build_dir.joinpath(BINARY_BASE_FILES_DIRNAME)
    if not boot_base_files_dir.exists():
        logging.debug(
            f'Creating boot base files directory {boot_base_files_dir.absolute()}')
        boot_base_files_dir.mkdir()

    return build_dir, base_files_dir, boot_base_files_dir


def dump_vmdb2_yaml(vmdb2_config, build_path):
    logging.info('Writing output yaml')
    miniserver_file_path = build_path.joinpath(MINISERVER_YAML)

    with open(miniserver_file_path.absolute(), 'w') as miniserver_file:
        yaml.dump(vmdb2_config, miniserver_file, default_flow_style=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Verbose print output.')
    parser.add_argument('-s', '--sudo', action='store_true',
                        help='Run vmdb2 with sudo.')
    parser.add_argument('-c', '--config',
                        default='config.yaml',
                        help='vmdb2 variables config yaml.')
    args = parser.parse_args()

    config_path = pathlib.Path(args.config)
    config = yaml.load(config_path.open(), Loader=yaml.FullLoader)

    vmdb2_args = []

    if args.verbose:
        logging.basicConfig(level=logging.INFO)
        vmdb2_args.append('--verbose')
    else:
        logging.basicConfig(level=logging.ERROR)

    build_path, base_path, boot_path = prepare_build_dir(config['build_dir'])
    render_base_files(config, base_path)

    boot_base_files_path = pathlib.Path('./boot_base_files/')
    for fl in boot_base_files_path.glob('*'):
        shutil.copy(str(fl.resolve()), str(boot_path.resolve()))

    vmdb2_config = load_vmdb2_steps(config)
    dump_vmdb2_yaml(vmdb2_config, build_path)

    base_default = f'{config["build_dir"]}/{config["img_name"]}'
    rootfs_default = pathlib.Path(f'{base_default}.tar.gz')
    output_default = pathlib.Path(f'{base_default}.img')
    logfile_default = pathlib.Path(f'{base_default}.log')
    vmdb2_default = pathlib.Path(f'{config["build_dir"]}/miniserver.yaml')

    vmdb2_args.append(f'--rootfs-tarball={rootfs_default}')
    vmdb2_args.append(f'--output={output_default}')
    vmdb2_args.append(f'--log={logfile_default}')
    vmdb2_args.append(f'{vmdb2_default}')

    # vmdb2 needs the log file to be created
    if not logfile_default.exists():
        logfile_default.touch()

    # vmdb2 must be called as root
    base_cmd = []
    if args.sudo:
        base_cmd.append('sudo')
    vmdb2_cmd = base_cmd + ['vmdb2']

    logging.info('Calling vmdb2')
    logging.info(f'{" ".join(vmdb2_cmd + vmdb2_args)}')

    result = subprocess.run(vmdb2_cmd + vmdb2_args,
                            stdout=sys.stdout,
                            stderr=sys.stderr)
    if not result.returncode == 0:
        sys.exit(1)

    result = subprocess.run(base_cmd + ['chmod', '644', output_default],
                            stdout=sys.stdout,
                            stderr=sys.stderr)
    if not result.returncode == 0:
        sys.exit(1)

    result = subprocess.run(base_cmd + ['chmod', '644', logfile_default],
                            stdout=sys.stdout,
                            stderr=sys.stderr)
    if not result.returncode == 0:
        sys.exit(1)
