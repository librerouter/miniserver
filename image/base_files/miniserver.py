#!/usr/bin/python3

import argparse
import os
import subprocess
import sys
import requests
from bs4 import BeautifulSoup


mender_release_file = "/etc/mender/artifact_info"
artifacts_url = "http://repo.librerouter.org/miniserver/artifacts/"
artifact_trailer = "artifact_v"

class Version():
    def __init__(self, version_str):
        artifact = version_str.split('.')
        self.h = int(artifact[0])
        self.l = int(artifact[1])

    def __eq__(self, other):
        if (self.h == other.h) and (self.l == other.l):
            return True
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __gt__(self, other):
        if self.h > other.h:
            return True
        elif self.h == other.h:
            if self.l > other.l:
                return True
            else:
                return False
        else:
            return False

    def __le__(self, other):
        return not self.__gt__(other)

    def __lt__(self, other):
        if self.h < other.h:
            return True
        elif self.h == other.h:
            if self.l < other.l:
                return True
            else:
                return False
        else:
            return False

    def __ge__(self, other):
        return not self.__lt__(other)

    def __str__(self):
        return f'{self.h}.{self.l}'


def get_remote_releases():
    artifacts = []
    page = requests.get(artifacts_url).text
    soup = BeautifulSoup(page, 'html.parser')
    hrefs = [j.get('href') for j in soup.find_all('a')]

    for href in hrefs:
        splited = href.split(artifact_trailer)
        if len(splited) == 2:
           artifact = Version(splited[1].replace('.mender', ''))
           artifacts.append(artifact)

    return artifacts

def get_local_release():
    if not os.path.exists(mender_release_file):
        raise FileExistsError

    with open(mender_release_file) as f:
        artifact_info = f.readline()
        return Version(artifact_info.split("artifact_name=")[1].split('_v')[1])

def get_args():
    parser = argparse.ArgumentParser(description='Manage miniserver updates.')
    parser.add_argument('--local_release', '-l', action='store_true', help='Print local release')
    parser.add_argument('--remote_release', '-r', action='store_true', help='Print remote release')
    parser.add_argument('--apply', '-a', help='Apply release (ex: 1.0)')
    parser.add_argument('--save', '-s', action='store_true', help='Save the current release as the default system.')

    if len(sys.argv) == 1:
        parser.print_help()
        exit(1)

    return parser.parse_args()

if __name__ == '__main__':
    args = get_args()

    if args.local_release:
        release = get_local_release()
        print(release)

    if args.remote_release:
        releases = get_remote_releases()
        for r in releases:
            print(r)

    if args.apply:
        release_url = f'{artifacts_url}{artifact_trailer}{args.apply}.mender'
        print(f"Retrieving last release: {release_url}")

        ret = subprocess.run(['sudo', 'mender', 'install', release_url],
                              stdout=sys.stdout, stderr=sys.stderr)

    if args.save:
        ret = subprocess.run(['sudo', 'mender', 'commit'],
                              stdout=sys.stdout, stderr=sys.stderr)
