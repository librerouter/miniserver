# MiniServer

Miniserver is a embedded platform to support LibreRouter mesh networks, A raspberry-pi (or alike) to hosts the CommunityHub at community networks.

The features expected from the minisever are:

* Community Hub: a simple ticket system where the community can organize tasks and keep communication focused on each problem identified, alongside a remote support system. Network administrators can decide to “push” a ticket from the CommunityHub upstream, which makes it visible to a global support team.
* Remote access by VPN: a decentralized remote support system to allow communities to keep up with network operation and maintenance.
* Remote secure upgrade of the system: OTA with Mender.io.
* Network metrics: dashboard with historical data of a mesh-network performance, which will allow to bootstrap automatic diagnosis of network health based on the operation parameters of real-world deployments.

## How to install and start using it in my community

Download the base image from [here](https://libremesh.org/miniserver/base_image/) and install it into a SD card using dd.

```bash
$ wget https://libremesh.org/miniserver/base_image/miniserver_full_image_<LAST_VERSION>.tar.gz
$ tar -xzf miniserver_full_image_{LAST_VERSION}.tar.gz
$ sudo dd if=miniserver_full_image_{LAST_VERSION}.tar.gz of=/dev/mmcblk0
```

You are done!

## How to upgrade the system

ssh into the miniserver and run:
```bash
admin@miniserver:~$ miniserver.py -l
1.0
```

This will print the local version of the system. To see the available releses run:
```bash
admin@miniserver:~$ miniserver.py -r
1.0
1.1
```

The to apply the selected release run:
```bash
admin@miniserver:~$ miniserver.py -a version
```

Reboot the system, if for any reason the upgrade wasent ok just reboot again an
it will return to the original version. If the upgrade goes well save it:
```bash
admin@miniserver:~ $ miniserver.py -s
```

If not, just rebooting the board rollback the system to the previous state.


## Development

The first feature that was addressed is the OTA upgrades. As is explained
[here](mender/README.md), we selected mender.io as the OTA upgrade system.
We selected mender because its integration whit uboot and the easy
integration whit Raspberry pi.

### Using the mender golden image for the RPi

As a first iteration, we use a the raspberry pi golden image provided by
mender. This image is already partitioned and has all the software needed. As
it is explained on the ["just mender"](mender/README.md) readme we took the
golden image provided by mender and "pump it up" ito be as we need.

The final image have the following structure:

![Image structure](doc/img/mender_image_structure.svg)

In the boot partition there are different files that boot the system. First the
board use the `bootcode.bin` to initialize and then looks for the
`config.txt`. In this file is defined the kernel to be boot. Here, if no kernel
is defined the boot code will look for `kernel7.img`. Part of the mender
conversion is to change this kernel image with the uboot binary. The uboot is
a custom build with some changes (the repo is
[here](https://github.com/mendersoftware/uboot-mender)) and here is where the
mender magic happens. It will use some special uboot variables to boot the
correct partition (rootfs_1 or rootfs_2).

![Boot process](doc/img/mender_boot_process.svg)

From here we can do releases in [the way that mender
does](https://docs.mender.io/2.0/architecture/mender-artifacts). We make some
changes and then release an artifact. Each miniserver downloads this artifact
that are available [here](https://libremesh.org/miniserver/artifacts/) with the mender client.


### Pure Debian system fully reproducible and automated

The mender image is great but depending on a golden image brings a main
problem, we need to distribute it and we don't have a reproducible way to 
build it and generate the artifacts. So, the objective is to build our own
image in an automated and reproducible way.

We started with the work of [Gunnar Wolf](Gunnar Wolf) that is building [the
official Debian images for rpi](https://raspi.debian.net/). He is using [vmdb2
and some makefiles](https://salsa.debian.org/raspi-team/image-specs) to do it. 

Using vmdb2 we developed a build system based in Python instead of Makefiles and
we added templating support (with jinja2) ([More info](image/README.md)). This
build system can produce a bootable image for the raspberry pi 3. By changing
the configuration files we can modify the image to add new packages or change 
configs. Releasing a new image only means doing a new tag on the repo!

This new system has a configuration file (`config.yaml`) where some parameters
of the system can be defined. Also, the build process is divided in various
files (`vmdb2_config/`) doing the build process more easy to understand.

### Adding mender to the debian system

To the debian image we need to add mender.io structure so it can be upgraded safely.
Mender provides a
[mender-convert](https://github.com/mendersoftware/mender-convert) script that
can be used to convert any Debian based image and add the uboot and needed
partitions. We tried but it doesn't work straight away.

The first problem was that the mender-convert script does override the
configurations on the boot partition. So, the board continues trying to boot
the Debian initramfs and kernel, but it breaks because mender DO change the
kernel cmdline file. Changing the `config.txt` to not define any initramfs 
or kernel fixes it.
As explained before, with no definition the bootcode looks for `kernel7.img`
that was installed by mender-convert and boots. Now the problem is that mender
uboot uses a `boot.scr` script that looks for a uImage or zImage file on the
rootfs. Debian doesn't use them so we need to address this problem.
