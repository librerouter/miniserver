# Secure OTA upgradeable system with mender

For the miniserver base system Debian is used. Looking for a secure way to
upgrade the distributed boards we found [Mender.io](https://mender.io). The
project provides 

# How was it made

Following the instructions [here](https://docs.mender.io/2.5/get-started/preparation/prepare-a-raspberry-pi-device).

We downloaded the base image:

```bash
$ wget https://d4o6e0uccgv40.cloudfront.net/2020-05-27-raspios-buster-lite-armhf/arm/2020-05-27-raspios-buster-lite-armhf-raspberrypi3-mender-2.4.0.img.xz
```

Deployed the image on the SD (Ensure to use the correct device):

```bash
$ sudo dd if=2020-05-27-raspios-buster-lite-armhf-raspberrypi3-mender-2.4.0.img of=/dev/mmcblk0 status=progress
```

This is the golden image provided by mender for raspberry pi. As a firs
approach we are going to use this as base image.

Some minor changes to enable debug console. Mount the boot partition and
change the bootcode binary to enable console:

```bash
$ sudo mount /dev/mmcblk0p1 
$ sudo sed -i -e "s/BOOT_UART=0/BOOT_UART=1/" /mnt/bootcode.bin
```
And enable uart for kernel:

```bash
echo "enable_uart=1" >> /mnt/config.txt
```

With this if we connect a USB-TTL serial adapter to uart1 on the raspberry
(GPIO14 to adapter rx, GPIO15 to adapter tx) we'll be able to see boot process
and use as first console.

Once connected, we login as pi user (pass raspberry) and do the bootstrapping
installation for the miniserver. First we enable the ssh connection:

```bash
pi@raspberry$ sudo systemctl enable ssh.service
pi@raspberry$ sudo systemctl start ssh.service
```

And from here we used a ethernet connection. We added the admin user, changed
the hostname and install some packages.

```bash
pi@raspberry$ sudo adduser admin 
# Configured admin with pass 1234
pi@raspberry$ sudo cp /etc/sudoers.d/010_pi-nopasswd /etc/sudoers.d/010_admin-nopasswd
pi@raspberry$ sudoedit /etc/sudoers.d/010_admin-nopasswd
# Changed the user name to admin allowing sudo without password
pi@raspberry$ sudo hostname
# Changed the hostname to miniserver
pi@raspberry$ sudo apt-intall vim tinc -y
pi@raspberry$ sudo vim /etc/hosts
# Changed the 127.0.1.1 to be miniserver
```

Now we generated the first artifact. First doing a snapshot of the system:

```bash
admin@miniserver$ sudo mender snapshot dump | ssh me@my_pc_ip /bin/sh -c 'cat > $HOME/root-part.ext4'
```

And then in a PC with mender tools installed;

```bash
$ mender-artifact write rootfs-image -f root-part.ext4 -n install_base_system -o snapshot_base_system.mender -t raspberrypi
```

The last step is to make this file accessible by http.

Taking a fresh sd with golden image we made the same procedure to enable uart
and booted the board with it. Once logged in we applied the artifact:

```bash
pi@raspberry:~$ sudo mender install http://my_pc_ip:8000/snapshot_base_system.mender 
```

Rebooting the board the system use the new artifact and then we can connect by
ssh. Now to close the artifact deploy we make a mender commit:

```bash
admin@miniserver:~ $ sudo mender commit
```

## Base release

To have a base release we dd this complete SD. From now on, using this image
as a golden image we can create new artifacts and distribute them.

The base image is available
[here](http://repo.libremesh.org/miniserver/miniserver_golden_image.img).Ensure
to use the correct device:

```bash
$ dd if=miniserver_golden_image.img of=/dev/mmcblk0 status=progress
```
